﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SpaService.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Http.Results;

namespace SpaService.Controllers
{
    public class BodyMeasuresController : ApiController
    {
        private SpaServiceContext db = new SpaServiceContext();

        // GET: api/BodyMeasures
        public IQueryable<BodyMeasure> GetBodyMeasures()
        {
            return db.BodyMeasures.Include(b => b.Customer);
        }

        // GET: api/BodyMeasures/5
        [ResponseType(typeof(BodyMeasure))]
        public async Task<IHttpActionResult> GetBodyMeasure(int id)
        {
            BodyMeasure bodyMeasure = await db.BodyMeasures.FindAsync(id);
            if (bodyMeasure == null)
            {
                return NotFound();
            }

            return Json( Ok(bodyMeasure));
        }

        // PUT: api/BodyMeasures/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBodyMeasure(int id, BodyMeasure bodyMeasure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bodyMeasure.ID)
            {
                return BadRequest();
            }

            db.Entry(bodyMeasure).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BodyMeasureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BodyMeasures
        [ResponseType(typeof(BodyMeasure))]
        public async Task<IHttpActionResult> PostBodyMeasure(BodyMeasure bodyMeasure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BodyMeasures.Add(bodyMeasure);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = bodyMeasure.ID }, bodyMeasure);
        }

        // DELETE: api/BodyMeasures/5
        [ResponseType(typeof(BodyMeasure))]
        public async Task<IHttpActionResult> DeleteBodyMeasure(int id)
        {
            BodyMeasure bodyMeasure = await db.BodyMeasures.FindAsync(id);
            if (bodyMeasure == null)
            {
                return NotFound();
            }

            db.BodyMeasures.Remove(bodyMeasure);
            await db.SaveChangesAsync();

            return Ok(bodyMeasure);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BodyMeasureExists(int id)
        {
            return db.BodyMeasures.Count(e => e.ID == id) > 0;
        }
    }
}