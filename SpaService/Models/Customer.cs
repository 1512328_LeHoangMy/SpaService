﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SpaService.Models
{
    public class Customer
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}