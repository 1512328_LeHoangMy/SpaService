﻿using System.ComponentModel.DataAnnotations;

namespace SpaService.Models
{
    public class BodyMeasure
    {
        public int ID { get; set; }
        [Required]
        public int Height { get; set; }
        public int Weight { get; set; }
        public int FatContent { get; set; }
        public int MuscleContent { get; set; }
        public int StomachFat { get; set; }
        public int BodyMass { get; set; }


        // Foreign Key
        public int CustomerID { get; set; }
        // Navigation property
        public Customer Customer { get; set; }
    }
}