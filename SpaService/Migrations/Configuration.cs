namespace SpaService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using SpaService.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<SpaService.Models.SpaServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SpaService.Models.SpaServiceContext context)
        {
            
            context.Customers.AddOrUpdate(x => x.Id,
        new Customer() { Id = 1, Name = "Jane Austen" },
        new Customer() { Id = 2, Name = "Charles Dickens" },
        new Customer() { Id = 3, Name = "Miguel de Cervantes" }
        );

            context.BodyMeasures.AddOrUpdate(x => x.ID,
                new BodyMeasure()
                {
                    ID = 1,
                    Height = 160,
                    Weight = 48,
                    FatContent = 1,
                    MuscleContent = 4,
                    StomachFat = 5,
                    BodyMass =3,
                    CustomerID = 1
                },
                new BodyMeasure()
                {
                    ID = 2,
                    Height = 180,
                    Weight = 70,
                    FatContent = 3,
                    MuscleContent = 5,
                    StomachFat = 2,
                    BodyMass = 3,
                    CustomerID = 2
                },
                new BodyMeasure()
                {
                    ID = 3,
                    Height = 155,
                    Weight = 46,
                    FatContent = 1,
                    MuscleContent = 9,
                    StomachFat = 6,
                    BodyMass = 2,
                    CustomerID = 3
                }
                
                );
        }
    }
}
