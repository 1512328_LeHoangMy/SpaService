namespace SpaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BodyMeasures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Height = c.Int(nullable: false),
                        Weight = c.Int(nullable: false),
                        FatContent = c.Int(nullable: false),
                        MuscleContent = c.Int(nullable: false),
                        StomachFat = c.Int(nullable: false),
                        BodyMass = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BodyMeasures", "CustomerID", "dbo.Customers");
            DropIndex("dbo.BodyMeasures", new[] { "CustomerID" });
            DropTable("dbo.Customers");
            DropTable("dbo.BodyMeasures");
        }
    }
}
